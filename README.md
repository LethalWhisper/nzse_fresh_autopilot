# README #

Fresh Autopilot App

### What is this repository for? ###


* Version 0.0.1


### How do I get set up? ###

* Clone into Android Developer Studio
* Make sure to use Simulator with SDK 25 / Android Device with newest Android

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact