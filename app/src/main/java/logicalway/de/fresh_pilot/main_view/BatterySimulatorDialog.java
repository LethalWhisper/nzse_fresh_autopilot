package logicalway.de.fresh_pilot.main_view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.widget.SeekBar;

import logicalway.de.fresh_pilot.R;
import logicalway.de.fresh_pilot.car.Battery;
import logicalway.de.fresh_pilot.car.FreshCar;

/**
 * Created by Alessandro on 12.05.17.
 */

public class BatterySimulatorDialog extends DialogFragment {



    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        FreshCar car = FreshCar.getInstance();

        final SeekBar seek = new SeekBar(this.getContext());
        seek.setProgress(car.getBattery().getPercentage());
        seek.setMax(Battery.BATTERY_MAX);
        seek.setKeyProgressIncrement(1);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(percentageMessage(car.getBattery().getPercentage()))
                .setView(seek)
                .setPositiveButton(R.string.change, (dialog, id) -> {
                    car.getBattery().setPercentage(seek.getProgress());
                    // Send the positive button event back to the host activity
                    mListener.onDialogPositiveClick(BatterySimulatorDialog.this);
                })
                .setNegativeButton(R.string.cancel, (dialog, id) -> {
                    // User cancelled the dialog
                });


        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();

        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                dialog.setMessage(percentageMessage(i));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return dialog;
    }

    public String percentageMessage(int percentage) {
        return getString(R.string.battery_change_percentage) + " (" + String.valueOf(percentage) + "%)";
    }
}
