package logicalway.de.fresh_pilot.main_view;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import logicalway.de.fresh_pilot.R;
import logicalway.de.fresh_pilot.car.CarSystem;
import logicalway.de.fresh_pilot.car.DoorState;
import logicalway.de.fresh_pilot.car.DoorType;
import logicalway.de.fresh_pilot.car.FreshCar;
import logicalway.de.fresh_pilot.route.RouteActivity;
import logicalway.de.fresh_pilot.route.service.POI;

public class MainMenu extends AppCompatActivity implements NoticeDialogListener, DoorChangedListener, DrivingSimulatorDialog.SimulationListener {


    private BatteryViewHandler batteryViewHandler;
    private DoorViewHandler doorViewHandler;
    private TemperatureViewHandler temperatureViewHandler;

    private FloatingActionButton powerButton;
    ValueAnimator powerButtonReadyAnimation;

    SharedPreferences preferences;


    //BUSINESS MODEL//
    private FreshCar car = FreshCar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        preferences = getSharedPreferences(getString(R.string.fresh_pref), MODE_PRIVATE);

        //power button
        powerButton = (FloatingActionButton) findViewById(R.id.powerButton);
        powerButton.setOnClickListener(l -> {

            handlePowerButton();
        });

        initDoors();
        CarSystem.getInstance().tryLoadData(preferences);
        doorViewHandler.updateViews();
        initElectrics();


    }

    @Override
    protected void onStop() {

        CarSystem.getInstance().saveData(preferences);
        super.onStop();
    }

    @Override
    protected void onDestroy() {

        CarSystem.getInstance().saveData(preferences);
        super.onDestroy();
    }

    private void handlePowerButton() {
        if (CarSystem.getInstance().isReadyToDrive(preferences)) {
            powerButtonReadyAnimation.end();
            //TODO simulate car

            //Open dialog
            DrivingSimulatorDialog drivingSimulatorDialog = new DrivingSimulatorDialog();
            drivingSimulatorDialog.setSimulationListener(this);
            drivingSimulatorDialog.show(getFragmentManager(), "drivingSimulator");
        } else {

            //show hint
            AlertDialog.Builder carNotReadyDialogBuilder = new AlertDialog.Builder(this);
            carNotReadyDialogBuilder.setTitle(R.string.car_not_ready_title)
                    .setPositiveButton(R.string.ok, null);
            carNotReadyDialogBuilder.create().show();

        }
    }

    private void initElectrics() {
        //initialize battery
        batteryViewHandler = new BatteryViewHandler((ImageButton) findViewById(R.id.battery),
                (TextView) findViewById(R.id.battery_state_label),
                this);

        batteryViewHandler.updateBatteryState(preferences);


        //initialize airCon
        temperatureViewHandler = new TemperatureViewHandler((ImageButton) findViewById(R.id.temperature),
                (TextView) findViewById(R.id.temperature_label), this);

        temperatureViewHandler.updateTemperatureState(preferences);
    }

    private void initDoors() {
        //initialize doors
        doorViewHandler = new DoorViewHandler(this, preferences);
        ToggleButton doorFLButton = (ToggleButton) findViewById(R.id.toggle_fl);
        doorFLButton.setTextOff(getString(R.string.label_door_closed));
        doorFLButton.setTextOn(getString(R.string.label_door_open));
        ToggleButton doorFRButton = (ToggleButton) findViewById(R.id.toggle_fr);
        doorFRButton.setTextOff(getString(R.string.label_door_closed));
        doorFRButton.setTextOn(getString(R.string.label_door_open));
        ToggleButton doorBLButton = (ToggleButton) findViewById(R.id.toggle_bl);
        doorBLButton.setTextOff(getString(R.string.label_door_closed));
        doorBLButton.setTextOn(getString(R.string.label_door_open));
        ToggleButton doorBRButton = (ToggleButton) findViewById(R.id.toggle_br);
        doorBRButton.setTextOff(getString(R.string.label_door_closed));
        doorBRButton.setTextOn(getString(R.string.label_door_open));
        ToggleButton trunkButton = (ToggleButton) findViewById(R.id.toggle_trunk);
        trunkButton.setTextOff(getString(R.string.label_door_closed));
        trunkButton.setTextOn(getString(R.string.label_door_open));

        car.openAllDoors();
        doorViewHandler.addDoorToggle(DoorType.FRONT_LEFT, doorFLButton)
                .addDoorToggle(DoorType.FRONT_RIGHT, doorFRButton)
                .addDoorToggle(DoorType.BACK_LEFT, doorBLButton)
                .addDoorToggle(DoorType.BACK_RIGHT, doorBRButton)
                .addDoorToggle(DoorType.TRUNK, trunkButton)
                .setPanicButton((ImageButton) findViewById(R.id.panic_button))
                .allDoors(DoorState.OPEN)
                .init();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    /**
     * Battery dialog listener positive callback
     * updates battery state (UI)
     */
    public void onDialogPositiveClick(DialogFragment dialog) {

        if (dialog.getTag().equals("batterySimulator")) {
            batteryViewHandler.updateBatteryState(preferences);
        } else if (dialog.getTag().equals("temperatureDialog")) {
            temperatureViewHandler.updateTemperatureState(preferences);
        }

        updatePowerButtonState();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        //do nothing
    }


    /**
     * Handle change of doors. If all doors are locked, <tt>locked</tt> will be true
     *
     * @param locked <tt>true</tt>, if all doors are locked, otherwise <tt>>false</tt>
     */
    @Override
    public void onDoorStateChange(boolean locked) {

        updatePowerButtonState();
    }


    public void switchToRoute(View view) {

        Intent intent = new Intent(this, RouteActivity.class);

        startActivity(intent);
    }

    /**
     * update presentation of the power button according to the cars' state
     */
    private void updatePowerButtonState() {

        boolean enabled = CarSystem.getInstance()
                .isReadyToDrive(getSharedPreferences(getString(R.string.fresh_pref), MODE_PRIVATE));

        int colorFrom = Color.rgb(20, 30, 200);
        int colorTo = Color.rgb(50, 180, 70);

        if (powerButtonReadyAnimation == null) {
            powerButtonReadyAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
            powerButtonReadyAnimation.setDuration(900); // milliseconds
            powerButtonReadyAnimation.setRepeatCount(ValueAnimator.INFINITE);
            powerButtonReadyAnimation.setRepeatMode(ValueAnimator.REVERSE);
            powerButtonReadyAnimation.addUpdateListener(animator -> powerButton.setImageTintList(ColorStateList.valueOf((int) animator.getAnimatedValue())));
        }


        if (!enabled) {
            powerButtonReadyAnimation.cancel();
            powerButton.setRippleColor(Color.rgb(100, 10, 10));
            powerButton.setImageTintList(ColorStateList.valueOf(Color.LTGRAY));
        } else {
            powerButtonReadyAnimation.start();
            powerButton.setRippleColor(Color.rgb(30, 200, 30));
        }


    }

    @Override
    public void onSimulationStart() {

    }

    @Override
    public void onSimulationPause() {

    }

    @Override
    public void onSimulationCancel() {

    }

    @Override
    public void onSimulationTick(long nthPeriod) {

        //Battery Logik
        car.getBattery().setPercentage(car.getBattery().getPercentage() - 1);
        System.out.println("Battery drained by 1 percent");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                batteryViewHandler.updateBatteryState(preferences);
            }
        });

    }
}
