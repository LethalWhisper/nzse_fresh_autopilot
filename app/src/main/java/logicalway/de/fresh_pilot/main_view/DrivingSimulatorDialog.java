package logicalway.de.fresh_pilot.main_view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.List;
import java.util.TimerTask;

import logicalway.de.fresh_pilot.R;
import logicalway.de.fresh_pilot.car.Battery;
import logicalway.de.fresh_pilot.car.CarSystem;
import logicalway.de.fresh_pilot.car.FreshCar;
import logicalway.de.fresh_pilot.car.RouteSimulator;
import logicalway.de.fresh_pilot.route.service.POI;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SHASSENP on 22.06.2017.
 */

public class DrivingSimulatorDialog extends DialogFragment {

    private SimulationListener simulationListener;
    private boolean isRunning = false;

    public static final long period = 1000;
    public static final int secsPerRouteChange = 5;
    private long nthPeriod = 0;

    private int progress = 0;
    private int noSteps = 0;
    private int routeIndex = 0;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            simulationListener = (SimulationListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement SimulationListener");
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        List<POI> route = initCarState();
        RouteSimulator simulator = new RouteSimulator(period);

        LinearLayout layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.VERTICAL);

        final SeekBar seek = new SeekBar(this.getContext());
        seek.setProgress(0);
        seek.setMax(Battery.BATTERY_MAX);
        seek.setKeyProgressIncrement(1);
        seek.setActivated(false);

        final TextView routeLabel = new TextView(getContext());
        routeLabel.setText(route.get(routeIndex++).getName());


        final Button simButton = new Button(this.getContext());
        simButton.setText(getString(R.string.start_sim));
        simButton.setOnClickListener((View l) -> {


            if (!isRunning) {
                simulationListener.onSimulationStart();
                simButton.setText(getString(R.string.paus_sim));

                simulator.start(createSimulationTask(route, simulator, seek, routeLabel));


            } else {
                simulationListener.onSimulationPause();
                simButton.setText(getString(R.string.start_sim));

                simulator.cancel();
            }

            isRunning = !isRunning;

        });

        layout.addView(routeLabel);
        layout.addView(seek);
        layout.addView(simButton);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


        builder.setMessage(R.string.route_sim_label)
                .setView(layout)
                .setPositiveButton(R.string.sim_stop, (dialog, id) -> {

                    simulator.cancel();
                    simulationListener.onSimulationCancel();
                });


        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();

        return dialog;
    }

    @NonNull
    private TimerTask createSimulationTask(List<POI> route, RouteSimulator simulator, SeekBar seek, TextView routeLabel) {
        return new TimerTask() {
            @Override
            public void run() {

                //Ziel anfahren
                handleRouteInfo(route, routeLabel);

                progress += 100 / (secsPerRouteChange * noSteps);
                seek.setProgress(progress);

                simulationListener.onSimulationTick(nthPeriod++);

                //as soon as 100, we still want to update once more
                if (progress == 100) {

                    handleSimulationEnd(simulator);

                }
            }
        };
    }

    private void handleRouteInfo(List<POI> route, TextView routeLabel) {
        if (nthPeriod % secsPerRouteChange == 0 && nthPeriod > 0) {
            //am Ziel angekommen
            System.out.println("Reached stopover / destination");
            int next = 0;

            if (routeIndex < route.size() - 1) {
                next = routeIndex++;
            } else {
                next = routeIndex;
            }
            routeLabel.setText(route.get(next).getName());
        } else {

            int prev = routeIndex - 1;
            int next;

            if (routeIndex < route.size()) {
                next = routeIndex;
            } else {
                next = route.size() - 1;
            }
            String text = route.get(prev).getName() + " - " + route.get(next).getName();
            routeLabel.setText(text);
        }
    }

    private void handleSimulationEnd(RouteSimulator simulator) {
        getActivity().runOnUiThread(() -> {
            AlertDialog confirmDialog;
            AlertDialog.Builder confirmBuilder = new AlertDialog.Builder(getContext());
            confirmBuilder.setTitle(R.string.sim_ended_label)
                    .setNeutralButton(getString(R.string.ok), (dialog, which) -> {
                        simulationListener.onSimulationCancel();
                        simulator.cancel();
                        dismiss();
                    });

            confirmDialog = confirmBuilder.create();
            confirmDialog.show();
        });
    }

    private List<POI> initCarState() {

        FreshCar car = FreshCar.getInstance();

        List<POI> route = CarSystem.getInstance()
                .getCurrentRoute(getActivity().getSharedPreferences(getString(R.string.fresh_pref), MODE_PRIVATE));

        noSteps = route.size() - 1;

        return route;
    }

    public interface SimulationListener {

        void onSimulationStart();

        void onSimulationPause();

        void onSimulationCancel();

        void onSimulationTick(long nthPeriod);

    }

    public SimulationListener getSimulationListener() {
        return simulationListener;
    }

    public void setSimulationListener(SimulationListener simulationListener) {
        this.simulationListener = simulationListener;
    }
}

