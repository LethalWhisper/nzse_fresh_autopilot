package logicalway.de.fresh_pilot.main_view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.widget.NumberPicker;

import logicalway.de.fresh_pilot.R;
import logicalway.de.fresh_pilot.car.FreshAirCon;
import logicalway.de.fresh_pilot.car.FreshCar;

/**
 * Created by Alessandro on 12.05.17.
 */

public class ChangeTemperatureDialog extends DialogFragment {


    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        FreshCar car = FreshCar.getInstance();

        final NumberPicker temperaturePicker = new NumberPicker(this.getContext());
        temperaturePicker.setMinValue(FreshAirCon.MIN_TEMP);
        temperaturePicker.setMaxValue(FreshAirCon.MAX_TEMP);

        temperaturePicker.setValue(car.getAirCon().getTemperature());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(tempMessage(car.getAirCon().getTemperature()))
                .setView(temperaturePicker)
                .setPositiveButton(R.string.change, (dialog, id) -> {
                    car.getAirCon().changeTemperature(temperaturePicker.getValue());
                    // Send the positive button event back to the host activity
                    mListener.onDialogPositiveClick(ChangeTemperatureDialog.this);
                })
                .setNegativeButton(R.string.cancel, (dialog, id) -> {
                    // User cancelled the dialog
                });


        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();

        temperaturePicker.setOnValueChangedListener((numberPicker1, i, i1) -> dialog.setMessage(tempMessage(i1)));

        return dialog;
    }

    public String tempMessage(float temp) {
        return getString(R.string.temperature_change_message) + " (" + String.valueOf(temp) + "°C)";
    }
}
