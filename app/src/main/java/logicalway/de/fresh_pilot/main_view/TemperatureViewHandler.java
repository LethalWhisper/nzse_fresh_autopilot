package logicalway.de.fresh_pilot.main_view;

import android.app.Activity;
import android.content.SharedPreferences;
import android.widget.ImageButton;
import android.widget.TextView;

import logicalway.de.fresh_pilot.R;
import logicalway.de.fresh_pilot.car.CarSystem;
import logicalway.de.fresh_pilot.car.FreshAirCon;
import logicalway.de.fresh_pilot.car.FreshCar;

/**
 * Created by Alessandro on 12.05.17.
 */

public class TemperatureViewHandler {

    private ImageButton temperatureButton;
    private TextView temperatureLabel;

    private Activity activity;

    private FreshCar car = FreshCar.getInstance();

    public TemperatureViewHandler(ImageButton temperatureButton, TextView temperatureLabel, Activity activity) {
        this.temperatureButton = temperatureButton;
        this.temperatureLabel = temperatureLabel;
        this.activity = activity;


        temperatureButton.setOnClickListener(view -> {

            new ChangeTemperatureDialog().show(activity.getFragmentManager(), "temperatureDialog");
        });
    }

    /**
     * Updates the temperature view
     */
    public void updateTemperatureState(SharedPreferences preferences) {

        int temp = car.getAirCon().getTemperature();
        this.temperatureLabel.setText(temperatureText(temp));

        int tempRange = FreshAirCon.MAX_TEMP - FreshAirCon.MIN_TEMP;
        int lowerBoundHighTemp = (tempRange / 3) * 2 + FreshAirCon.MIN_TEMP;
        int lowerBoundMedTemp = tempRange / 3 + FreshAirCon.MIN_TEMP;

        //High temp if temp <= max and temp > max/2
        if (Float.compare(temp, FreshAirCon.MAX_TEMP) <= 0 && temp > lowerBoundHighTemp) {
            this.temperatureButton.setBackgroundResource(R.drawable.temp_high);
        } else if (Float.compare(temp, lowerBoundHighTemp) >= 0 && temp > lowerBoundMedTemp) {
            this.temperatureButton.setBackgroundResource(R.drawable.temp_medium);
        } else {
            this.temperatureButton.setBackgroundResource(R.drawable.temp_low);
        }

        CarSystem.getInstance().saveData(preferences);
    }

    String temperatureText(int temp) {

        return String.format(activity.getResources().getConfiguration().getLocales().get(0), "%d°C", temp);
    }
}
