package logicalway.de.fresh_pilot.main_view;

import android.content.SharedPreferences;
import android.widget.ImageButton;
import android.widget.ToggleButton;

import java.util.HashMap;
import java.util.Map;

import logicalway.de.fresh_pilot.R;
import logicalway.de.fresh_pilot.car.CarSystem;
import logicalway.de.fresh_pilot.car.DoorState;
import logicalway.de.fresh_pilot.car.DoorType;
import logicalway.de.fresh_pilot.car.FreshCar;

/**
 * Created by Alessandro on 12.05.17.
 */

public class DoorViewHandler {

    private Map<DoorType, ToggleButton> doorToggleList;
    private ImageButton panicButton;
    private FreshCar car = FreshCar.getInstance();

    private SharedPreferences preferences;


    private DoorChangedListener changedListener;

    public DoorViewHandler(DoorChangedListener listener, SharedPreferences preferences) {

        changedListener = listener;
        doorToggleList = new HashMap<>();

        this.preferences = preferences;
    }

    /**
     * Adds a door toggle-button reference.
     *
     * @param type             the door the button controls
     * @param doorToggleButton the reference to the controlling button
     * @return {@link DoorViewHandler} self
     */
    public DoorViewHandler addDoorToggle(DoorType type, ToggleButton doorToggleButton) {

        doorToggleList.put(type, doorToggleButton);

        return this;
    }

    /**
     * Set the panic button view-reference
     *
     * @param panicButton the reference to the panic-button on the view
     * @return {@link DoorViewHandler} self
     */
    public DoorViewHandler setPanicButton(ImageButton panicButton) {
        this.panicButton = panicButton;

        return this;
    }


    /**
     * Change the state of all door buttons at once.
     * WARNING: This does <b>NOT</b> manipulate the {@link FreshCar}'s doors lock-statuses!
     *
     * @param state State to which to set the buttons to. {@link DoorState#OPEN} means that all buttons
     *              will be set to <tt>ON</tt>, {@link DoorState#CLOSED} means <tt>OFF</tt>.
     * @return {@link DoorViewHandler} self
     */
    public DoorViewHandler allDoors(DoorState state) {

        boolean checked = state.equals(DoorState.OPEN);
        doorToggleList.values().forEach(val -> val.setChecked(checked));
        changedListener.onDoorStateChange(!checked);
        return this;
    }

    /**
     * Initializes the handler and the event listeners of each added door as well
     * as the event listener of the panic button.
     * After each addition of a door you have to call init() again.
     */
    public void init() {

        //door click-event listeners
        doorToggleList.forEach((type, toggle) -> toggle.setOnClickListener(view -> {

            //1 before toggling this door check if toggling this button would "violate" the all-locked status of the car
            if (car.allDoorsClosed()) {
                setLockIconOpen(true); //one door is open so the panicButton indicates open (= insecure)
            }
            car.toggleDoor(type);
            //2 after toggle, check if all doors are closed now, after this door was toggled
            //to show the locked icon
            if (car.allDoorsClosed()) {
                allDoors(DoorState.CLOSED);
                setLockIconOpen(false);
            }

            CarSystem.getInstance().saveData(preferences);


        }));

        //panic button click-event listener
        panicButton.setOnClickListener(view -> {

            if (car.allDoorsClosed()) {
                car.openAllDoors();
                allDoors(DoorState.OPEN);
                setLockIconOpen(true);
            } else {
                car.closeAllDoors();
                allDoors(DoorState.CLOSED);
                setLockIconOpen(false);
            }

            CarSystem.getInstance().saveData(preferences);


        });
    }

    public void updateViews() {

        doorToggleList.forEach((type, button) -> {
            button.setChecked(car.getStateOf(type) != DoorState.CLOSED);
        });

        if (car.allDoorsClosed()) {
            setLockIconOpen(false);
        } else {
            setLockIconOpen(true);
        }

    }

    private void setLockIconOpen(boolean open) {

        if (open) {
            panicButton.setBackgroundResource(R.drawable.lock_open);
        } else {
            panicButton.setBackgroundResource(R.drawable.lock_closed);
        }
    }


}
