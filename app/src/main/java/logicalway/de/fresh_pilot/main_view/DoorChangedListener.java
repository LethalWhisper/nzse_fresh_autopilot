package logicalway.de.fresh_pilot.main_view;

/**
 * Created by Alessandro on 18.05.17.
 */

public interface DoorChangedListener {

    void onDoorStateChange(boolean locked);
}
