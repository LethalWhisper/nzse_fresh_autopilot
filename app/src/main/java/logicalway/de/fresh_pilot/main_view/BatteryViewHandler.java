package logicalway.de.fresh_pilot.main_view;


import android.app.Activity;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import logicalway.de.fresh_pilot.R;
import logicalway.de.fresh_pilot.car.CarSystem;
import logicalway.de.fresh_pilot.car.FreshCar;

/**
 * Created by Alessandro on 12.05.17.
 */

public class BatteryViewHandler {

    private ImageButton batteryButton;
    private TextView batteryStateLabel;

    private FreshCar car = FreshCar.getInstance();


    public BatteryViewHandler(ImageButton batteryButton, TextView batteryStateLabel, Activity activity) {
        this.batteryButton = batteryButton;
        this.batteryStateLabel = batteryStateLabel;

        batteryButton.setOnClickListener((View view) -> {
            BatterySimulatorDialog dialog = new BatterySimulatorDialog();

            dialog.show(activity.getFragmentManager(), "batterySimulator");
        });
    }

    public void updateBatteryState(SharedPreferences preferences) {

        int batteryDrawableId;

        switch (car.getBattery().getBatteryState()) {


            case FULL:
                batteryDrawableId = R.drawable.battery_full;
                break;
            case P80:
                batteryDrawableId = R.drawable.battery_80;
                break;
            case P60:
                batteryDrawableId = R.drawable.battery_60;
                break;
            case P40:
                batteryDrawableId = R.drawable.battery_40;
                break;
            case P20:
                batteryDrawableId = R.drawable.battery_20;
                break;
            case EMPTY:
            default:
                batteryDrawableId = R.drawable.battery_empty;
                break;

        }

        batteryButton.setBackgroundResource(batteryDrawableId);

        String batteryPercentage = String.valueOf(car.getBattery().getPercentage()) + " %";
        batteryStateLabel.setText(batteryPercentage);

        CarSystem.getInstance().saveData(preferences);

    }

}
