package logicalway.de.fresh_pilot.route.service;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

import logicalway.de.fresh_pilot.R;

/**
 * Created by Alessandro on 23.05.17.
 */

public class DownloadIconTask extends AsyncTask<String, Integer, Drawable> {

    private final WeakReference<ImageView> imageViewReference;


    public DownloadIconTask(ImageView imageView) {

        this.imageViewReference = new WeakReference<>(imageView);
    }

    @Override
    protected Drawable doInBackground(String... urls) {

        if (urls[0] != null) {
            return new RouteEngine().getPointOfInterestIcon(urls[0]);
        } else {
            return null;
        }

    }

    @Override
    protected void onPostExecute(Drawable drawable) {

        if (imageViewReference.get() != null) {
            if (isCancelled()) {
                drawable = null;
            }

            ImageView v = imageViewReference.get();

            if (drawable == null) {
                v.setImageResource(R.drawable.ic_not_interested_blue_grey_200_48dp);
            } else {
                v.setImageDrawable(drawable);
            }


        }
    }

    public WeakReference<ImageView> getImageViewReference() {
        return imageViewReference;
    }
}