package logicalway.de.fresh_pilot.route.service;

/**
 * Created by Alessandro on 23.05.17.
 */

public class POI {

    private String type;
    private String name;
    private String address;
    private double lat;
    private double lon;
    private String icon;

    private RouteType routeType;

    public POI(String type, String name, String address, double lat, double lon, String icon) {
        this.type = type;
        this.name = name;
        this.address = address;
        this.lat = lat;
        this.lon = lon;
        this.icon = icon;

        this.routeType = RouteType.NONE;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getIcon() {
        return icon;
    }

    public RouteType getRouteType() {
        return routeType;
    }

    public void setRouteType(RouteType routeType) {
        this.routeType = routeType;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        POI poi = (POI) o;

        if (Double.compare(poi.lat, lat) != 0) return false;
        if (Double.compare(poi.lon, lon) != 0) return false;
        if (!type.equals(poi.type)) return false;
        if (!name.equals(poi.name)) return false;
        if (!address.equals(poi.address)) return false;
        return icon != null ? icon.equals(poi.icon) : poi.icon == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = type.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + address.hashCode();
        temp = Double.doubleToLongBits(lat);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lon);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        return result;
    }
}
