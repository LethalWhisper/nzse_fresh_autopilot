package logicalway.de.fresh_pilot.route;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import logicalway.de.fresh_pilot.route.service.DownloadIconTask;

/**
 * Created by Alessandro on 23.05.17.
 */

public class POIViewHolder extends RecyclerView.ViewHolder {


    private View view;
    private int section;

    private DownloadIconTask downloadIconTask;

    public POIViewHolder(View itemView) {
        super(itemView);
        this.view = itemView;
    }


    public void onItemSelected() {

        if (view != null) {
            view.setBackgroundColor(Color.argb(30, 10, 10, 100));
        }
    }

    public void onItemClear() {

        if (view != null) {
            view.setBackgroundColor(Color.argb(100, 255, 255, 255));
        }
    }

    public int getSection() {
        return section;
    }

    public void setSection(int section) {
        this.section = section;
    }

    public DownloadIconTask getDownloadIconTask() {
        return downloadIconTask;
    }

    public void setDownloadIconTask(DownloadIconTask downloadIconTask) {
        this.downloadIconTask = downloadIconTask;
    }
}
