package logicalway.de.fresh_pilot.route;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import logicalway.de.fresh_pilot.R;
import logicalway.de.fresh_pilot.route.service.DownloadIconTask;
import logicalway.de.fresh_pilot.route.service.ItemTouchHelperAdapter;
import logicalway.de.fresh_pilot.route.service.POI;

/**
 * Created by Alessandro on 23.05.17.
 */

public class ExtendedPoiAdapter extends RecyclerView.Adapter<POIViewHolder>
        implements ItemTouchHelperAdapter {


    public static final int ROUTE_SEP = 0;
    public static final int ROUTE = 1;
    public static final int AVAILABLE_SEP = 2;
    public static final int AVAILABLE = 3;

    private LayoutInflater vi;
    private Context context;

    private Map<Integer, List<POI>> poiMap;
    private List<String> separators = Collections.emptyList();

    public ExtendedPoiAdapter(Context context, Map<Integer, List<POI>> poiMap) {
        super();
        this.poiMap = poiMap;
        this.context = context;

        separators = Arrays.asList(context.getString(R.string.title_route), context.getString(R.string.title_available_pois));
    }


    public int getCount(int type) {

        if (poiMap.get(type) != null) {
            return poiMap.get(type).size();
        }

        return 0;
    }

    @Override
    public POIViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v;

        if (vi == null) {
            vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (viewType == ROUTE_SEP || viewType == AVAILABLE_SEP) {
            v = vi.inflate(R.layout.separator, null);
        } else {
            v = vi.inflate(R.layout.poi_list_item, null);
        }

        return new POIViewHolder(v);
    }

    @Override
    public void onBindViewHolder(POIViewHolder holder, int position) {

        int enrouteC = getCount(ROUTE);
        int availableC = getCount(AVAILABLE);
        int viewType = holder.getItemViewType();

        View v = holder.itemView;

        //route section || available section
        if (position == 0 || position == getLastRouteIndex()) {

            TextView lView = (TextView) v.findViewById(R.id.separator_text);

            int section = position == 0 ? 0 : 1;
            lView.setText(separators.get(section));

            holder.setSection(section);


        }
        //route || available
        else {
            POI poi;

            if (viewType == AVAILABLE) {
                poi = poiMap.get(viewType).get(position - enrouteC - separators.size());
            } else {
                poi = poiMap.get(viewType).get(position - 1);
            }


            if (poi != null) {

                ImageView imageView = (ImageView) v.findViewById(R.id.imageView);
                TextView nameTextView = (TextView) v.findViewById(R.id.poi_name);
                TextView addressTextView = (TextView) v.findViewById(R.id.poi_address);
                TextView typeTextView = (TextView) v.findViewById(R.id.poi_type);
                v.setBackgroundColor(Color.WHITE);

                nameTextView.setText(poi.getName());
                addressTextView.setText(poi.getAddress());
                String typeStr = "Typ: " + poi.getType();
                typeTextView.setText(typeStr);

                holder.setDownloadIconTask(new DownloadIconTask(imageView));
                holder.getDownloadIconTask().execute(poi.getIcon());

                if (imageView.getDrawable() == null) {
                    imageView.setImageResource(R.drawable.ic_not_interested_blue_grey_200_48dp);
                }
            }

            changeViewHolderAppearance(holder, position, enrouteC);
        }

    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return getCount(ROUTE) + getCount(AVAILABLE) + separators.size();
    }

    @Override
    public int getItemViewType(int position) {

        int rCount = getCount(ROUTE);

        if (position == 0) {
            return ROUTE_SEP;
        }
        if (position == rCount + 1) {
            return AVAILABLE_SEP;
        }

        if (position <= rCount) {
            return ROUTE;
        }

        return AVAILABLE;
    }


    @Override
    public void onItemFinishedMoving(RecyclerView.ViewHolder holder, RecyclerView.ViewHolder oldHolder, int toPos, int fromPos) {


        moveItem(fromPos, toPos, holder, oldHolder);

        notifyDataSetChanged(); //Bad practice, since images will get reloaded as well


    }

    private int getLastRouteIndex() {

        return getCount(ROUTE) + 1;

    }

    private void changeViewHolderAppearance(RecyclerView.ViewHolder holder, int pos, int routeCount) {

        TextView labelView = (TextView) holder.itemView.findViewById(R.id.poi_name);

        //START
        if (pos == 1) {
            holder.itemView.setBackgroundColor(Color.argb(50, 55, 220, 40));

            String str = poiMap.get(ROUTE).get(0).getName();
            str = str + " " + context.getString(R.string.poi_label_start);
            labelView.setText(str);

            //STOP OVER
        } else if (pos < routeCount) {
            holder.itemView.setBackgroundColor(Color.argb(50, 150, 150, 150));

            String str = poiMap.get(ROUTE).get(pos - 1).getName();
            str = str + " (" + context.getString(R.string.stopover_label) + " " + (pos - 1) + ")";
            labelView.setText(str);

            //DESTINATION
        } else if (pos == routeCount) {
            holder.itemView.setBackgroundColor(Color.argb(50, 220, 55, 40));

            String str = poiMap.get(ROUTE).get(pos - 1).getName();
            str = str + " (" + context.getString(R.string.poi_label_target) + ")";
            labelView.setText(str);
        }
    }

    /**
     * Naive and inefficient method to move items around
     *
     * @param fromPosition
     * @param toPosition
     * @param holder
     * @param oldHolder
     */
    private void moveItem(int fromPosition, int toPosition, RecyclerView.ViewHolder holder, RecyclerView.ViewHolder oldHolder) {

        int toType;
        int fromType = holder.getItemViewType();

        if (toPosition == 0) {
            toPosition = 1;
        }

        //we move something into the route section
        if (toPosition < getLastRouteIndex() || fromPosition >= getLastRouteIndex()) {
            toType = ROUTE;
        } else {
            toType = AVAILABLE;
        }

        POI poi;
        //we move from one section to another
        if (fromType != toType) {

            int section;
            int toSection;
            if (fromType == AVAILABLE) {
                section = 1;
            } else {
                section = 0;
            }
            if (toType == AVAILABLE) {
                toSection = 1;
            } else {
                toSection = 0;
            }

            poi = poiMap.get(fromType).get(getTrueItemIndex(section, fromPosition));
            poiMap.get(fromType).remove(poi);
            poiMap.get(toType).add(getTrueItemIndex(toSection, toPosition), poi);

        } else {

            if (fromType == ROUTE) {
                Collections.swap(poiMap.get(ROUTE), fromPosition - 1, toPosition - 1);
            } else {
                Collections.swap(poiMap.get(AVAILABLE), getTrueItemIndex(1, fromPosition), getTrueItemIndex(1, toPosition));
            }
        }

        if (toPosition < 1) { //we don't move the first section row!!!
            notifyItemMoved(fromPosition, 1);
        } else {
            notifyItemMoved(fromPosition, toPosition);
        }

    }

    private int getNumberOfSections() {

        return poiMap.keySet().size();
    }

    private int getTrueItemIndex(int section, int position) {

        if (section == 0) {
            return position - 1; //section row takes one row, therefore dec by 1
        } else {
            int i = getNumberOfSections() - 1;
            int offset = section + 1; // +1 because of first section row
            while (i > 0) {
                if (poiMap.get(i) != null) {
                    offset += poiMap.get(i).size();
                    i--;
                }
            }

            return position - offset;
        }

    }
}

