package logicalway.de.fresh_pilot.route.service;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Alessandro on 23.05.17.
 */

public interface ItemTouchHelperAdapter {

    void onItemFinishedMoving(RecyclerView.ViewHolder holder, RecyclerView.ViewHolder oldHolder, int toPos, int fromPos);
}