package logicalway.de.fresh_pilot.route.service;

/**
 * Created by Alessandro on 23.05.17.
 */

public enum RouteType {

    NONE,
    START,
    DESTINATION,
    STOPOVER,
}
