package logicalway.de.fresh_pilot.route.service;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.StrictMode;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by Alessandro on 23.05.17.
 */

public class RouteEngine {

    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";

    private static final String URL_STRING = "https://www.fbi.h-da.de/fileadmin/personal/h.wiedling/daten/";

    public List<POI> loadRoute(List<POI> availablePOIs, SharedPreferences preferences) {
        int i = 0;
        int hash = preferences.getInt(("r0"), -1);

        List<POI> resultList = new ArrayList<>();

        while (hash != -1) {

            int finalHash = hash;
            POI p = availablePOIs
                    .stream()
                    .filter(poi -> poi.hashCode() == finalHash)
                    .findFirst().orElse(null);
            if (p != null) {
                resultList.add(p);
            }
            i++;
            hash = preferences.getInt(("r" + i), -1);
        }

        return resultList;
    }

    public List<POI> loadRoute(SharedPreferences preferences) {

        return loadRoute(getPointsOfInterest(), preferences);
    }


    public List<POI> getPointsOfInterest() {

        Gson gson = new Gson();
        POI[] poiList = gson.fromJson(getStringResponse("poi.txt"), POI[].class);

        return new ArrayList<>(Arrays.asList(poiList));
    }

    public Drawable getPointOfInterestIcon(String img) {

        URL url;
        Drawable d;
        //try to resolve the given url
        try {
            url = new URL("https://www.fbi.h-da.de/fileadmin/personal/h.wiedling/daten/" + img);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }

        //try to download the resource
        try {
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            d = Drawable.createFromStream(con.getInputStream(), null);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return d;
    }


    private String getStringResponse(String urlSuffix) {
        //TODO: fix  --> Use AsyncTask
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        StringBuilder response = new StringBuilder();

        //Prepare the URL and the connection
        URL u;

        try {
            u = new URL(URL_STRING + "poi.txt");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }

        try {
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                //Get the Stream reader ready
                BufferedReader input = new BufferedReader(new InputStreamReader(conn.getInputStream()), 8192);

                //Loop through the return data and copy it over to the response object to be processed
                String line = null;

                while ((line = input.readLine()) != null) {
                    response.append(line);
                }

                input.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response.toString();
    }
}
