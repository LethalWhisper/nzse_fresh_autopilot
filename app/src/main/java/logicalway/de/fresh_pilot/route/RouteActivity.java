package logicalway.de.fresh_pilot.route;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import logicalway.de.fresh_pilot.R;
import logicalway.de.fresh_pilot.route.service.ItemTouchCallback;
import logicalway.de.fresh_pilot.route.service.POI;
import logicalway.de.fresh_pilot.route.service.RouteEngine;

public class RouteActivity extends Activity {

    // This is the Adapter being used to display the list's data
    private ExtendedPoiAdapter mAdapter;
    private Map<Integer, List<POI>> poiMap;
    private SharedPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler);

        RouteEngine engine = new RouteEngine();


        preferences = getSharedPreferences(getString(R.string.fresh_pref), MODE_PRIVATE);

        //init poiMap
        poiMap = new HashMap<>();

        //load existing route if available
        List<POI> available = engine.getPointsOfInterest();
        List<POI> routeList = engine.loadRoute(available, preferences);

        available.removeAll(routeList);

        poiMap.put(ExtendedPoiAdapter.AVAILABLE, available);
        poiMap.put(ExtendedPoiAdapter.ROUTE, routeList);

        //set content
        mAdapter = new ExtendedPoiAdapter(this, poiMap);

        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ItemTouchCallback callback = new ItemTouchCallback(mAdapter);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(recyclerView);


    }


    @Override
    protected void onStop() {
        super.onStop();

        //save the state of the route

        IntStream.range(0, poiMap.get(ExtendedPoiAdapter.ROUTE).size()).forEach(i -> {

            POI p = poiMap.get(ExtendedPoiAdapter.ROUTE).get(i);
            preferences.edit()
                    .putInt("r" + i, p.hashCode())
                    .apply();
        });
    }

}
