package logicalway.de.fresh_pilot.car;

/**
 * Created by Alessandro on 28.04.17.
 */

public enum DoorState {

    OPEN,
    CLOSED
}
