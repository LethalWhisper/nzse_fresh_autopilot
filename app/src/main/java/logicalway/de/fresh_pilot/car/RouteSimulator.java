package logicalway.de.fresh_pilot.car;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by SHASSENP on 22.06.2017.
 */

public class RouteSimulator {

    private Timer timer;
    private long period;

    public RouteSimulator(long period) {
        this.period = period;
    }

    public boolean isStarted() {

        return timer != null;
    }

    public void start(TimerTask task) {

        timer = new Timer("simuTimer");
        timer.scheduleAtFixedRate(task, 0, period);


    }

    public void cancel() {

        if (timer != null) {
            timer.cancel();
        }

        timer = null;
    }


}
