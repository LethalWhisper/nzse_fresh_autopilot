package logicalway.de.fresh_pilot.car;

/**
 * Created by Alessandro on 18.05.17.
 */

public enum CarErrorState {

    DOORS_OPEN,
    BATTERY_LOW,
    NO_ROUTE

}
