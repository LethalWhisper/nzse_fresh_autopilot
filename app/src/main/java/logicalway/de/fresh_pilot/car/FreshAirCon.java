package logicalway.de.fresh_pilot.car;

import java.io.Serializable;

/**
 * Created by Alessandro on 28.04.17.
 */

public class FreshAirCon implements Serializable {

    public static final int MAX_TEMP = 28;
    public static final int MIN_TEMP = 18;

    private static final long serialVersionUID = -7473933060288136422L;

    private static FreshAirCon instance;

    private int temperature = 20;


    //CONSTRUCTOR//
    private FreshAirCon() {
    }


    //INIT//

    /**
     * Get the singleton instance of the air conditioner
     *
     * @return the instance of airCon
     */
    public static FreshAirCon getInstance() {
        if (instance == null) {
            instance = new FreshAirCon();
        }

        return instance;
    }

    public void changeTemperature(int newTemp) {

        if (temperature > MAX_TEMP) {
            throw new IllegalArgumentException("Temperature value exceeds maximum of: " + MAX_TEMP);
        } else if (temperature < MIN_TEMP) {
            throw new IllegalArgumentException("Temperature value exceeds minimum of:  " + MIN_TEMP);
        } else {
            temperature = newTemp;
        }
    }

    public int getTemperature() {
        return temperature;
    }

}
