package logicalway.de.fresh_pilot.car;

/**
 * Created by Alessandro on 28.04.17.
 */

public enum DoorType {

    FRONT_LEFT,
    FRONT_RIGHT,
    BACK_LEFT,
    BACK_RIGHT,
    TRUNK
}
