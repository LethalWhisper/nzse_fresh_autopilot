package logicalway.de.fresh_pilot.car;

/**
 * Created by Alessandro on 12.05.17.
 */

public class Door {

    private DoorType type;
    private DoorState state;

    public Door(DoorType type, DoorState state) {
        this.type = type;
        this.state = state;
    }

    public DoorType getType() {
        return type;
    }

    public void setType(DoorType type) {
        this.type = type;
    }

    public DoorState getState() {
        return state;
    }

    public void setState(DoorState state) {
        this.state = state;
    }
}
