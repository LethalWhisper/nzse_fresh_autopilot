package logicalway.de.fresh_pilot.car;

/**
 * Created by Alessandro on 12.05.17.
 */

public enum BatteryState {

    FULL,
    P80,
    P60,
    P40,
    P20,
    EMPTY
}
