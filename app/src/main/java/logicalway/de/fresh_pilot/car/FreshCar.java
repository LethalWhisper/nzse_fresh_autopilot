package logicalway.de.fresh_pilot.car;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Created by Alessandro on 28.04.17.
 */

public class FreshCar implements Serializable {

    private static final long serialVersionUID = -1959818091213872472L;
    private static FreshCar instance = new FreshCar(); //tread safe init

    private List<Door> doors;
    private FreshAirCon airCon;
    private Battery battery;


    //CONSTRUCTOR//
    private FreshCar() {
        //private constructor

        doors = new ArrayList<>();
        doors.add(new Door(DoorType.FRONT_LEFT, DoorState.OPEN));
        doors.add(new Door(DoorType.FRONT_RIGHT, DoorState.OPEN));
        doors.add(new Door(DoorType.BACK_LEFT, DoorState.OPEN));
        doors.add(new Door(DoorType.BACK_RIGHT, DoorState.OPEN));
        doors.add(new Door(DoorType.TRUNK, DoorState.OPEN));

        airCon = FreshAirCon.getInstance();
        battery = Battery.getInstance();

    }


    //METHODS//

    /**
     * Retrieve singleton instance of the car
     */
    public static FreshCar getInstance() {
        return instance;
    }


    public boolean allDoorsClosed() {

        return doors.stream().allMatch(door -> door.getState().equals(DoorState.CLOSED));
    }

    public boolean allDoorsOpen() {

        return doors.stream().allMatch(door -> door.getState().equals(DoorState.OPEN));
    }

    /**
     * Closes all doors
     */
    public void closeAllDoors() {
        doors.forEach(door -> door.setState(DoorState.CLOSED));
    }

    /**
     * Open all doors
     */
    public void openAllDoors() {
        doors.forEach(door -> door.setState(DoorState.OPEN));
    }

    /**
     * Toggles the state of the given door.
     * If the door was open before, it is now closed and vice versa.
     *
     * @param doorType door to toggle the state of
     */

    public void toggleDoor(DoorType doorType) {

        Optional<Door> found = doors.parallelStream()
                .filter(door -> door.getType().equals(doorType)).findFirst();

        if (found.isPresent()) {

            Door d = found.get();

            if (d.getState() == DoorState.CLOSED) {
                d.setState(DoorState.OPEN);
            } else {
                d.setState(DoorState.CLOSED);
            }
        } else {
            throw new IllegalArgumentException("The specified door does not exist for some reason!");
        }
    }

    public DoorState getStateOf(DoorType doorType) {
        try {
            return doors.stream().filter(door -> door.getType().equals(doorType)).findFirst().orElseThrow(RuntimeException::new).getState();
        } catch (Throwable throwable) {
            throw new IllegalArgumentException("Unknown Door");
        }
    }

    public Battery getBattery() {
        return battery;
    }

    public FreshAirCon getAirCon() {
        return airCon;
    }

    public List<Door> getDoors() {
        return doors;
    }
}
