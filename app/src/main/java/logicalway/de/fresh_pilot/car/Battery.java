package logicalway.de.fresh_pilot.car;

/**
 * Created by Alessandro on 12.05.17.
 */

public class Battery {


    public static final int BATTERY_MAX = 100;
    public static final int BATTERY_MIN = 0;


    private static Battery instance;
    private int percentage;

    private Battery() {
        this.percentage = 100;
    }

    public static Battery getInstance() {
        if (instance == null) {
            instance = new Battery();
        }

        return instance;
    }


    /**
     * Returns the rough battery state based on the actual percentage of energy left
     * in the battery
     *
     * @return rough {@link BatteryState} of the battery
     */
    public BatteryState getBatteryState() {

        BatteryState state = BatteryState.EMPTY;

        if (percentage > 80) {
            state = BatteryState.FULL;
        } else if (percentage <= 80 && percentage > 60) {
            state = BatteryState.P80;
        } else if (percentage <= 60 && percentage > 40) {
            state = BatteryState.P60;
        } else if (percentage <= 40 && percentage > 20) {
            state = BatteryState.P40;
        } else if (percentage <= 20 && percentage > 5) {
            state = BatteryState.P20;
        }

        return state;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }
}
