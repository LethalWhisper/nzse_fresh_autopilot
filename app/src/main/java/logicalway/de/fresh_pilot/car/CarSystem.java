package logicalway.de.fresh_pilot.car;

import android.content.SharedPreferences;

import java.util.List;

import logicalway.de.fresh_pilot.main_view.DrivingSimulatorDialog;
import logicalway.de.fresh_pilot.route.service.POI;
import logicalway.de.fresh_pilot.route.service.RouteEngine;

/**
 * Created by Alessandro on 12.05.17.
 */

public class CarSystem {

    public static final String CAR_BATTERY = "car_battery";
    public static final String CAR_TEMPERATURE = "car_temperature";
    public static final String CAR_DOOR_PREFIX = "car_door_";

    private static final CarSystem instance = new CarSystem();

    private static RouteEngine routeEngine;

    private CarSystem() {
        //empty
        routeEngine = new RouteEngine();
    }

    private FreshCar car = FreshCar.getInstance();


    public static CarSystem getInstance() {
        return instance;
    }

    public void tryLoadData(SharedPreferences preferences) {

        if (preferences.contains(CAR_BATTERY)) {
            car.getBattery().setPercentage(preferences.getInt(CAR_BATTERY, 50));
        }
        if (preferences.contains(CAR_TEMPERATURE)) {
            car.getAirCon().changeTemperature(preferences.getInt(CAR_TEMPERATURE, 20));
        }

        car.getDoors().forEach(door -> {

            if (preferences.contains(door.getType().name())) {
                door.setState(DoorState.values()[preferences.getInt(door.getType().name(), 0)]);
            }
        });
    }

    public void saveData(SharedPreferences preferences) {

        preferences.edit().putInt(CAR_BATTERY, car.getBattery().getPercentage()).apply();
        preferences.edit().putInt(CAR_TEMPERATURE, car.getAirCon().getTemperature()).apply();

        car.getDoors()
                .forEach(door -> preferences.edit().putInt(door.getType().name(), door.getState().ordinal()).apply());


    }

    public boolean isReadyToDrive(SharedPreferences preferences) {

        List<POI> route = routeEngine.loadRoute(preferences);

        return car.allDoorsClosed()
                && !car.getBattery().getBatteryState().equals(BatteryState.EMPTY)
                && route.size() >= 2
                && isBatterySufficient(route.size(), DrivingSimulatorDialog.secsPerRouteChange);
        //TODO route set
    }

    public List<POI> getCurrentRoute(SharedPreferences preferences) {

        return routeEngine.loadRoute(preferences);
    }

    public boolean isBatterySufficient(int routePoints, long secsPerRouteChange) {

        int percentage = car.getBattery().getPercentage();

        return (percentage - routePoints * secsPerRouteChange) >= 0;
    }


}
